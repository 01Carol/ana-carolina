﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo2.DB.Prova.DB.Resistencia
{
    class ResistenciaBusiness
    {
        public int Salvar(ResistenciaDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.Mensagem == string.Empty)
            {
                throw new ArgumentException("Mensagem é obrigatório.");
            }

            ResistenciaDatabase db = new ResistenciaDatabase();
            return db.Salvar(dto);
        }

        public List<ResistenciaDTO> Listar()
        {
            ResistenciaDatabase db = new ResistenciaDatabase();
            return db.Listar();
        }
    }
}
